#!/bin/bash

##
# 任意のディレクトリ以下にある全てのgitリポジトリを最新にする
##

# config
#TARGET_DIR=~/workCopy/git/github_tamurayk
TARGET_DIR=


find ${TARGET_DIR} -type d -name .git | xargs -n 1 dirname | sort | while read line; do echo $line && cd $line && git checkout master && git pull && cd ${TARGET_DIR}; done
