## overview

GitHub の 任意のオーガニゼーションからリポジトリをすべて clone する

## prerequisites

* `jq` をインストール
	* `brew install jq`

## usage

1. `git clone https://bitbucket.org/tamurayk/git_clone_all_from_github.git`
2. `cd git_clone_all_from_github`
3. `clone_all.sh` の config 部分を設定
4. `sh clone_all.sh`