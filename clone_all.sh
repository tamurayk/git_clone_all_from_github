#!/bin/bash

##
# GitHub の 任意のオーガニゼーションのリポジトリをすべて clone する
#
#
# GitHubのAPIを使用し、対象オーガニゼーションのリポジトリ一覧を取得
#  tips : curl -u で basic認証のかかったサイトにアクセス
# ↓
# jq で パース
# ↓
# git clone をループ
#
##

# config
USER_NAME=''
USER_PASSWD=''

TARGET_ORGS=''


# デフォルトでは 30 件までしか取得しない為、per_page オプションを使用
curl -u ${USER_NAME}:${USER_PASSWD} https://api.github.com/orgs/${TARGET_ORGS}/repos?per_page=100 | jq -r '.[].clone_url' |
while read -r repo; do
  echo "clone ${repo} ..."
  git clone ${repo}
done